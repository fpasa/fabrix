# Fabrix Documentation

Fabrix is a protocol for display management under unix,
similar to Wayland.

## Overview

Fabrix governs the interaction between the compositor and
client applications. The compositor is in charge of
managing the displays and controlling the graphics hardware,
while client applications interact with the compositor
in order to achieve their goals of displaying useful
information to the user.

## Principles

These are the principles governing the design of the
Fabrix protocol (in order of important):

- Simplicity: implementing clients and compositors
  should require as little code as possible.
- Completeness: Fabrix tries to be complete for it's
  use case: desktop and mobile display management.
  This means that no feature that's considered essential
  on a modern system should be missing.
- Security: Fabrix is a modern and secure protocol.
  It aims to
- Efficiency: Minimize the work client and compositors
  have to do and communication between them.

## High Level Design

Compositor and clients communicate with each other through
a socket, usually a unix socket.

Through this socket, the clients send requests to the compositor
and the compositor emits events for the clients to process.
The messages wire format uses protocol buffers.
