module sample_client

go 1.17

replace wayland/compositor/registry => ../libraries/compositor/registry

require wayland/compositor/registry v0.0.0-00010101000000-000000000000 // indirect
