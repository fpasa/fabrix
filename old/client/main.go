package main

import (
	"net"
	"fmt"
	"time"
)

func main() {
	conn, err := net.Dial("unix", "/run/user/1000/wayland-0")
	if err != nil {
		panic(err)
	}
	conn.Write([]byte("\x00\x00\x00\x01\x00\x0c\x00\x00\x00\x00\x00\x02"))
	conn.Write([]byte("\x00\x00\x00\x01\x00\x0c\x00\x01\x00\x00\x00\x03"))

	time.Sleep(1)

	var a []byte
	n, err := conn.Read(a)
	if err != nil {
		panic(err)
	}
	fmt.Println(n)
	fmt.Println(a)
}
