package registry

import "net"

func Listen() error {
	listener, err := net.Listen("unix", "tmp.sock")
	if err != nil {
		return err
	}

	for {
		connection, err := l.Accept()
		if err != nil {
			// TODO: better error handling
			return err
		}

		go func() {
			connection.Close()
		}
	}
}
