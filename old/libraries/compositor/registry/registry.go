package registry

type Registry struct {
	objects map[uint32]interface{}
}

func NewRegistry() *Registry {
	registry := new(Registry)
	return registry
}
