package protocol

import "encoding/gob"

func Decode(message []byte, object interface{}) error {
	decoder := gob.NewDecoder(message)
	return decoder.Decode(object)
}

func Encode(object interface{}) ([]byte, error) {
	var buffer bytes.Buffer
	encoder := gob.NewEncoder(buffer)
	err := encoder.Encode(object)
	return buffer, nil
}
