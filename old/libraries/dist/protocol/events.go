package protocol


type DisplayErrorEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    ObjectId uint32
    Code uint32
    Message string
}

type DisplayDeleteIdEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Id uint32
}

type RegistryGlobalEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Name uint32
    Interface string
    Version uint32
}

type RegistryGlobalRemoveEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Name uint32
}

type CallbackDoneEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    CallbackData uint32
}

type ShmFormatEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Format uint32
}

type BufferReleaseEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16

}

type DataOfferOfferEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    MimeType string
}

type DataOfferSourceActionsEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    SourceActions uint32
}

type DataOfferActionEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    DndAction uint32
}

type DataSourceTargetEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    MimeType string
}

type DataSourceSendEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    MimeType string
    Fd uint32
}

type DataSourceCancelledEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16

}

type DataSourceDndDropPerformedEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16

}

type DataSourceDndFinishedEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16

}

type DataSourceActionEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    DndAction uint32
}

type DataDeviceDataOfferEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Id uint32
}

type DataDeviceEnterEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Serial uint32
    Surface uint32
    X float32
    Y float32
    Id uint32
}

type DataDeviceLeaveEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16

}

type DataDeviceMotionEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Time uint32
    X float32
    Y float32
}

type DataDeviceDropEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16

}

type DataDeviceSelectionEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Id uint32
}

type ShellSurfacePingEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Serial uint32
}

type ShellSurfaceConfigureEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Edges uint32
    Width int32
    Height int32
}

type ShellSurfacePopupDoneEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16

}

type SurfaceEnterEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Output uint32
}

type SurfaceLeaveEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Output uint32
}

type SeatCapabilitiesEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Capabilities uint32
}

type SeatNameEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Name string
}

type PointerEnterEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Serial uint32
    Surface uint32
    SurfaceX float32
    SurfaceY float32
}

type PointerLeaveEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Serial uint32
    Surface uint32
}

type PointerMotionEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Time uint32
    SurfaceX float32
    SurfaceY float32
}

type PointerButtonEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Serial uint32
    Time uint32
    Button uint32
    State uint32
}

type PointerAxisEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Time uint32
    Axis uint32
    Value float32
}

type PointerFrameEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16

}

type PointerAxisSourceEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    AxisSource uint32
}

type PointerAxisStopEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Time uint32
    Axis uint32
}

type PointerAxisDiscreteEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Axis uint32
    Discrete int32
}

type KeyboardKeymapEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Format uint32
    Fd uint32
    Size uint32
}

type KeyboardEnterEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Serial uint32
    Surface uint32
    Keys []byte
}

type KeyboardLeaveEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Serial uint32
    Surface uint32
}

type KeyboardKeyEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Serial uint32
    Time uint32
    Key uint32
    State uint32
}

type KeyboardModifiersEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Serial uint32
    ModsDepressed uint32
    ModsLatched uint32
    ModsLocked uint32
    Group uint32
}

type KeyboardRepeatInfoEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Rate int32
    Delay int32
}

type TouchDownEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Serial uint32
    Time uint32
    Surface uint32
    Id int32
    X float32
    Y float32
}

type TouchUpEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Serial uint32
    Time uint32
    Id int32
}

type TouchMotionEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Time uint32
    Id int32
    X float32
    Y float32
}

type TouchFrameEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16

}

type TouchCancelEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16

}

type TouchShapeEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Id int32
    Major float32
    Minor float32
}

type TouchOrientationEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Id int32
    Orientation float32
}

type OutputGeometryEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    X int32
    Y int32
    PhysicalWidth int32
    PhysicalHeight int32
    Subpixel int32
    Make string
    Model string
    Transform int32
}

type OutputModeEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Flags uint32
    Width int32
    Height int32
    Refresh int32
}

type OutputDoneEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16

}

type OutputScaleEvent struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Factor int32
}

