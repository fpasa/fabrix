package protocol


type DisplaySyncRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Callback uint32
}

type DisplayGetRegistryRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Registry uint32
}

type RegistryBindRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Name uint32
    Id uint32
}

type CompositorCreateSurfaceRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Id uint32
}

type CompositorCreateRegionRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Id uint32
}

type ShmPoolCreateBufferRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Id uint32
    Offset int32
    Width int32
    Height int32
    Stride int32
    Format uint32
}

type ShmPoolDestroyRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16

}

type ShmPoolResizeRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Size int32
}

type ShmCreatePoolRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Id uint32
    Fd uint32
    Size int32
}

type BufferDestroyRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16

}

type DataOfferAcceptRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Serial uint32
    MimeType string
}

type DataOfferReceiveRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    MimeType string
    Fd uint32
}

type DataOfferDestroyRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16

}

type DataOfferFinishRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16

}

type DataOfferSetActionsRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    DndActions uint32
    PreferredAction uint32
}

type DataSourceOfferRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    MimeType string
}

type DataSourceDestroyRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16

}

type DataSourceSetActionsRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    DndActions uint32
}

type DataDeviceStartDragRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Source uint32
    Origin uint32
    Icon uint32
    Serial uint32
}

type DataDeviceSetSelectionRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Source uint32
    Serial uint32
}

type DataDeviceReleaseRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16

}

type DataDeviceManagerCreateDataSourceRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Id uint32
}

type DataDeviceManagerGetDataDeviceRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Id uint32
    Seat uint32
}

type ShellGetShellSurfaceRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Id uint32
    Surface uint32
}

type ShellSurfacePongRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Serial uint32
}

type ShellSurfaceMoveRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Seat uint32
    Serial uint32
}

type ShellSurfaceResizeRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Seat uint32
    Serial uint32
    Edges uint32
}

type ShellSurfaceSetToplevelRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16

}

type ShellSurfaceSetTransientRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Parent uint32
    X int32
    Y int32
    Flags uint32
}

type ShellSurfaceSetFullscreenRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Method uint32
    Framerate uint32
    Output uint32
}

type ShellSurfaceSetPopupRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Seat uint32
    Serial uint32
    Parent uint32
    X int32
    Y int32
    Flags uint32
}

type ShellSurfaceSetMaximizedRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Output uint32
}

type ShellSurfaceSetTitleRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Title string
}

type ShellSurfaceSetClassRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Class string
}

type SurfaceDestroyRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16

}

type SurfaceAttachRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Buffer uint32
    X int32
    Y int32
}

type SurfaceDamageRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    X int32
    Y int32
    Width int32
    Height int32
}

type SurfaceFrameRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Callback uint32
}

type SurfaceSetOpaqueRegionRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Region uint32
}

type SurfaceSetInputRegionRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Region uint32
}

type SurfaceCommitRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16

}

type SurfaceSetBufferTransformRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Transform int32
}

type SurfaceSetBufferScaleRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Scale int32
}

type SurfaceDamageBufferRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    X int32
    Y int32
    Width int32
    Height int32
}

type SeatGetPointerRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Id uint32
}

type SeatGetKeyboardRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Id uint32
}

type SeatGetTouchRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Id uint32
}

type SeatReleaseRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16

}

type PointerSetCursorRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Serial uint32
    Surface uint32
    HotspotX int32
    HotspotY int32
}

type PointerReleaseRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16

}

type KeyboardReleaseRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16

}

type TouchReleaseRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16

}

type OutputReleaseRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16

}

type RegionDestroyRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16

}

type RegionAddRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    X int32
    Y int32
    Width int32
    Height int32
}

type RegionSubtractRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    X int32
    Y int32
    Width int32
    Height int32
}

type SubcompositorDestroyRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16

}

type SubcompositorGetSubsurfaceRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Id uint32
    Surface uint32
    Parent uint32
}

type SubsurfaceDestroyRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16

}

type SubsurfaceSetPositionRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    X int32
    Y int32
}

type SubsurfacePlaceAboveRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Sibling uint32
}

type SubsurfacePlaceBelowRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
    Sibling uint32
}

type SubsurfaceSetSyncRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16

}

type SubsurfaceSetDesyncRequest struct {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16

}

