package protocol

import "encoding/gob"


func DecodeDisplayRequest(message []byte, opcode int) (interface{}, error) {
    var err error
    switch opcode { 
        case 0:
            request := DisplaySyncRequest{}
            err = Decode(request)
            return request, err
        case 1:
            request := DisplayGetRegistryRequest{}
            err = Decode(request)
            return request, err
    }
}

func DecodeRegistryRequest(message []byte, opcode int) (interface{}, error) {
    var err error
    switch opcode { 
        case 0:
            request := RegistryBindRequest{}
            err = Decode(request)
            return request, err
    }
}

func DecodeCallbackRequest(message []byte, opcode int) (interface{}, error) {
    var err error
    switch opcode { 
    }
}

func DecodeCompositorRequest(message []byte, opcode int) (interface{}, error) {
    var err error
    switch opcode { 
        case 0:
            request := CompositorCreateSurfaceRequest{}
            err = Decode(request)
            return request, err
        case 1:
            request := CompositorCreateRegionRequest{}
            err = Decode(request)
            return request, err
    }
}

func DecodeShmPoolRequest(message []byte, opcode int) (interface{}, error) {
    var err error
    switch opcode { 
        case 0:
            request := ShmPoolCreateBufferRequest{}
            err = Decode(request)
            return request, err
        case 1:
            request := ShmPoolDestroyRequest{}
            err = Decode(request)
            return request, err
        case 2:
            request := ShmPoolResizeRequest{}
            err = Decode(request)
            return request, err
    }
}

func DecodeShmRequest(message []byte, opcode int) (interface{}, error) {
    var err error
    switch opcode { 
        case 0:
            request := ShmCreatePoolRequest{}
            err = Decode(request)
            return request, err
    }
}

func DecodeBufferRequest(message []byte, opcode int) (interface{}, error) {
    var err error
    switch opcode { 
        case 0:
            request := BufferDestroyRequest{}
            err = Decode(request)
            return request, err
    }
}

func DecodeDataOfferRequest(message []byte, opcode int) (interface{}, error) {
    var err error
    switch opcode { 
        case 0:
            request := DataOfferAcceptRequest{}
            err = Decode(request)
            return request, err
        case 1:
            request := DataOfferReceiveRequest{}
            err = Decode(request)
            return request, err
        case 2:
            request := DataOfferDestroyRequest{}
            err = Decode(request)
            return request, err
        case 3:
            request := DataOfferFinishRequest{}
            err = Decode(request)
            return request, err
        case 4:
            request := DataOfferSetActionsRequest{}
            err = Decode(request)
            return request, err
    }
}

func DecodeDataSourceRequest(message []byte, opcode int) (interface{}, error) {
    var err error
    switch opcode { 
        case 0:
            request := DataSourceOfferRequest{}
            err = Decode(request)
            return request, err
        case 1:
            request := DataSourceDestroyRequest{}
            err = Decode(request)
            return request, err
        case 2:
            request := DataSourceSetActionsRequest{}
            err = Decode(request)
            return request, err
    }
}

func DecodeDataDeviceRequest(message []byte, opcode int) (interface{}, error) {
    var err error
    switch opcode { 
        case 0:
            request := DataDeviceStartDragRequest{}
            err = Decode(request)
            return request, err
        case 1:
            request := DataDeviceSetSelectionRequest{}
            err = Decode(request)
            return request, err
        case 2:
            request := DataDeviceReleaseRequest{}
            err = Decode(request)
            return request, err
    }
}

func DecodeDataDeviceManagerRequest(message []byte, opcode int) (interface{}, error) {
    var err error
    switch opcode { 
        case 0:
            request := DataDeviceManagerCreateDataSourceRequest{}
            err = Decode(request)
            return request, err
        case 1:
            request := DataDeviceManagerGetDataDeviceRequest{}
            err = Decode(request)
            return request, err
    }
}

func DecodeShellRequest(message []byte, opcode int) (interface{}, error) {
    var err error
    switch opcode { 
        case 0:
            request := ShellGetShellSurfaceRequest{}
            err = Decode(request)
            return request, err
    }
}

func DecodeShellSurfaceRequest(message []byte, opcode int) (interface{}, error) {
    var err error
    switch opcode { 
        case 0:
            request := ShellSurfacePongRequest{}
            err = Decode(request)
            return request, err
        case 1:
            request := ShellSurfaceMoveRequest{}
            err = Decode(request)
            return request, err
        case 2:
            request := ShellSurfaceResizeRequest{}
            err = Decode(request)
            return request, err
        case 3:
            request := ShellSurfaceSetToplevelRequest{}
            err = Decode(request)
            return request, err
        case 4:
            request := ShellSurfaceSetTransientRequest{}
            err = Decode(request)
            return request, err
        case 5:
            request := ShellSurfaceSetFullscreenRequest{}
            err = Decode(request)
            return request, err
        case 6:
            request := ShellSurfaceSetPopupRequest{}
            err = Decode(request)
            return request, err
        case 7:
            request := ShellSurfaceSetMaximizedRequest{}
            err = Decode(request)
            return request, err
        case 8:
            request := ShellSurfaceSetTitleRequest{}
            err = Decode(request)
            return request, err
        case 9:
            request := ShellSurfaceSetClassRequest{}
            err = Decode(request)
            return request, err
    }
}

func DecodeSurfaceRequest(message []byte, opcode int) (interface{}, error) {
    var err error
    switch opcode { 
        case 0:
            request := SurfaceDestroyRequest{}
            err = Decode(request)
            return request, err
        case 1:
            request := SurfaceAttachRequest{}
            err = Decode(request)
            return request, err
        case 2:
            request := SurfaceDamageRequest{}
            err = Decode(request)
            return request, err
        case 3:
            request := SurfaceFrameRequest{}
            err = Decode(request)
            return request, err
        case 4:
            request := SurfaceSetOpaqueRegionRequest{}
            err = Decode(request)
            return request, err
        case 5:
            request := SurfaceSetInputRegionRequest{}
            err = Decode(request)
            return request, err
        case 6:
            request := SurfaceCommitRequest{}
            err = Decode(request)
            return request, err
        case 7:
            request := SurfaceSetBufferTransformRequest{}
            err = Decode(request)
            return request, err
        case 8:
            request := SurfaceSetBufferScaleRequest{}
            err = Decode(request)
            return request, err
        case 9:
            request := SurfaceDamageBufferRequest{}
            err = Decode(request)
            return request, err
    }
}

func DecodeSeatRequest(message []byte, opcode int) (interface{}, error) {
    var err error
    switch opcode { 
        case 0:
            request := SeatGetPointerRequest{}
            err = Decode(request)
            return request, err
        case 1:
            request := SeatGetKeyboardRequest{}
            err = Decode(request)
            return request, err
        case 2:
            request := SeatGetTouchRequest{}
            err = Decode(request)
            return request, err
        case 3:
            request := SeatReleaseRequest{}
            err = Decode(request)
            return request, err
    }
}

func DecodePointerRequest(message []byte, opcode int) (interface{}, error) {
    var err error
    switch opcode { 
        case 0:
            request := PointerSetCursorRequest{}
            err = Decode(request)
            return request, err
        case 1:
            request := PointerReleaseRequest{}
            err = Decode(request)
            return request, err
    }
}

func DecodeKeyboardRequest(message []byte, opcode int) (interface{}, error) {
    var err error
    switch opcode { 
        case 0:
            request := KeyboardReleaseRequest{}
            err = Decode(request)
            return request, err
    }
}

func DecodeTouchRequest(message []byte, opcode int) (interface{}, error) {
    var err error
    switch opcode { 
        case 0:
            request := TouchReleaseRequest{}
            err = Decode(request)
            return request, err
    }
}

func DecodeOutputRequest(message []byte, opcode int) (interface{}, error) {
    var err error
    switch opcode { 
        case 0:
            request := OutputReleaseRequest{}
            err = Decode(request)
            return request, err
    }
}

func DecodeRegionRequest(message []byte, opcode int) (interface{}, error) {
    var err error
    switch opcode { 
        case 0:
            request := RegionDestroyRequest{}
            err = Decode(request)
            return request, err
        case 1:
            request := RegionAddRequest{}
            err = Decode(request)
            return request, err
        case 2:
            request := RegionSubtractRequest{}
            err = Decode(request)
            return request, err
    }
}

func DecodeSubcompositorRequest(message []byte, opcode int) (interface{}, error) {
    var err error
    switch opcode { 
        case 0:
            request := SubcompositorDestroyRequest{}
            err = Decode(request)
            return request, err
        case 1:
            request := SubcompositorGetSubsurfaceRequest{}
            err = Decode(request)
            return request, err
    }
}

func DecodeSubsurfaceRequest(message []byte, opcode int) (interface{}, error) {
    var err error
    switch opcode { 
        case 0:
            request := SubsurfaceDestroyRequest{}
            err = Decode(request)
            return request, err
        case 1:
            request := SubsurfaceSetPositionRequest{}
            err = Decode(request)
            return request, err
        case 2:
            request := SubsurfacePlaceAboveRequest{}
            err = Decode(request)
            return request, err
        case 3:
            request := SubsurfacePlaceBelowRequest{}
            err = Decode(request)
            return request, err
        case 4:
            request := SubsurfaceSetSyncRequest{}
            err = Decode(request)
            return request, err
        case 5:
            request := SubsurfaceSetDesyncRequest{}
            err = Decode(request)
            return request, err
    }
}

