from pathlib import Path
from shutil import copytree, rmtree
from typing import Dict, Iterable, List, Tuple
from xml.etree import ElementTree
from xml.etree.ElementTree import Element
from dataclasses import dataclass

PROTOCOL_SPEC_PATH = Path("wayland.xml")
TEMPLATES_PATH = Path("templates")
OUTPUT_PATH = Path("dist")

REQUESTS_OUTPUT_PATH = Path("dist/protocol/requests.go")
EVENTS_OUTPUT_PATH = Path("dist/protocol/events.go")
REQUESTS_ENCODING_OUTPUT_PATH = Path("dist/protocol/requests_encoding.go")
STRUCT_TEMPLATE = """
type {name} struct {{
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
{args}
}}
"""

GO_TYPE_MAP = {
    "new_id": "uint32",
    "object": "uint32",
    "uint": "uint32",
    "int": "int32",
    "string": "string",
    "fixed": "float32",
    "array": "[]byte",
    # TODO
    "fd": "uint32",
}

DECODE_FUNC_TEMPLATE = """
func Decode{request}Request(message []byte, opcode int) (interface{{}}, error) {{
    var err error
    switch opcode {{ {cases}
    }}
}}
"""
REQUEST_CASE_TEMPLATE = """
        case {case}:
            request := {struct}{{}}
            err = Decode(request)
            return request, err
""".rstrip()


@dataclass
class Request:
    name: str
    opcode: int
    args: Dict[str, str]


@dataclass
class Event:
    name: str
    opcode: int
    args: Dict[str, str]


@dataclass
class Interface:
    name: str
    requests: Dict[int, Request]
    events: Dict[int, Event]


def main() -> None:
    if OUTPUT_PATH.exists():
        rmtree(OUTPUT_PATH)
    copytree(TEMPLATES_PATH, OUTPUT_PATH)

    interfaces = parse_interfaces()

    write_code(interfaces)


def parse_interfaces() -> List[Interface]:
    tree = ElementTree.parse(PROTOCOL_SPEC_PATH)

    interfaces = []
    for interface_node in tree.findall("interface"):
        requests = parse_requests(interface_node)
        events = parse_events(interface_node)
        interface = Interface(
            name=interface_node.attrib["name"],
            requests=requests,
            events=events,
        )
        interfaces.append(interface)

    return interfaces


def parse_requests(interface_node: Element) -> Dict[str, Request]:
    requests = {}
    for req_opcode, req in enumerate(interface_node.findall("request")):
        args = {
            arg.attrib["name"]: arg.attrib["type"]
            for arg in req.findall("arg")
        }
        requests[req_opcode] = Request(
            name=req.attrib["name"],
            opcode=req_opcode,
            args=args,
        )

    return requests


def parse_events(interface_node: Element) -> Dict[str, Event]:
    events = {}
    for req_opcode, req in enumerate(interface_node.findall("event")):
        args = {
            arg.attrib["name"]: arg.attrib["type"]
            for arg in req.findall("arg")
        }
        events[req_opcode] = Event(
            name=req.attrib["name"],
            opcode=req_opcode,
            args=args,
        )

    return events


def write_code(interfaces: List[Interface]) -> None:
    structs = get_request_structs(interfaces)
    transform_file(
        REQUESTS_OUTPUT_PATH,
        structs=structs,
    )

    structs = get_event_structs(interfaces)
    transform_file(
        EVENTS_OUTPUT_PATH,
        structs=structs,
    )

    decode_funcs = get_decode_functions(interfaces)
    transform_file(
        REQUESTS_ENCODING_OUTPUT_PATH,
        decode_funcs=decode_funcs,
    )


def get_request_structs(interfaces: List[Interface]) -> str:
    structs = []
    for interface, req in iter_requests(interfaces):
        name = req_struct_name(interface, req, "Request")

        args = []
        for arg_name, arg_type in req.args.items():
            go_name = name_to_camel_case(arg_name)
            go_type = GO_TYPE_MAP[arg_type]
            args.append(f"    {go_name} {go_type}")

        struct = STRUCT_TEMPLATE.format(name=name, args="\n".join(args))
        structs.append(struct)

    return "".join(structs)


def get_event_structs(interfaces: List[Interface]) -> str:
    structs = []
    for interface, req in iter_events(interfaces):
        name = req_struct_name(interface, req, "Event")

        args = []
        for arg_name, arg_type in req.args.items():
            go_name = name_to_camel_case(arg_name)
            go_type = GO_TYPE_MAP[arg_type]
            args.append(f"    {go_name} {go_type}")

        struct = STRUCT_TEMPLATE.format(name=name, args="\n".join(args))
        structs.append(struct)

    return "".join(structs)


def get_decode_functions(interfaces: List[Interface]) -> str:
    funcs = []
    for interface in interfaces:
        requests = []
        for req in interface.requests.values():
            requests.append(
                REQUEST_CASE_TEMPLATE.format(
                    case=req.opcode,
                    struct=req_struct_name(interface, req, "Request")
                )
            )

        funcs.append(
            DECODE_FUNC_TEMPLATE.format(
                request=name_to_camel_case(interface.name),
                cases="".join(requests),
            )
        )

    return "".join(funcs)


def req_struct_name(interface: Interface, request: Request, suffix: str) -> str:
    interface_name = name_to_camel_case(interface.name)
    request_name = name_to_camel_case(request.name)
    name = f"{interface_name}{request_name}{suffix}"
    return name


def iter_requests(interfaces: List[Interface]) -> Iterable[Tuple[Interface, Request]]:
    for interface in interfaces:
        for req in interface.requests.values():
            yield interface, req


def iter_events(interfaces: List[Interface]) -> Iterable[Tuple[Interface, Request]]:
    for interface in interfaces:
        for req in interface.events.values():
            yield interface, req


def transform_file(path: Path, **args) -> None:
    content = path.read_text()
    content = content.format(**args)
    path.write_text(content)


def name_to_camel_case(name: str) -> str:
    # Remove the wl_ in front of the name
    if name[:3] == "wl_":
        name = name[3:]

    camel_case = ""
    capital = True
    for char in name:
        if char == "_":
            capital = True
            continue

        if capital:
            camel_case += char.upper()
            capital = False
        else:
            camel_case += char

    return camel_case


if __name__ == "__main__":
    main()
