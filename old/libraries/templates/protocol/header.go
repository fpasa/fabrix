package protocol

import "encoding/gob"

struct MessageHeader {
	ObjectID uint32
	MessageSize uint16
	OpCode uint16
}

func DecodeHeader(message []byte) (interface{}, error) {
	header := MessageHeader{}
	decoder := gob.NewDecoder(message)
	err := decoder.Decode(header)
	return header, err
}
