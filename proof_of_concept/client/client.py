import os
import socket
import logging
import struct

from protocol.requests.buffer_pb2 import CreateBuffer, DestroyBuffer, BufferType

SOCKET_ADDRESS = os.environ["FABRIX_ADDR"]
REQUEST_CLASSES = {CreateBuffer: 0, DestroyBuffer: 1}


def main(address: str):
    logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.INFO)
    logging.info("client started")

    msg = CreateBuffer()
    msg.id = 1
    msg.type = BufferType.MEMORY
    msg.pool = "hello"

    with socket.socket(socket.AF_UNIX, socket.SOCK_STREAM) as sock:
        sock.connect(address)

        sock.send(struct.pack("I", 0) + msg.SerializeToString())


if __name__ == "__main__":
    main(SOCKET_ADDRESS)
