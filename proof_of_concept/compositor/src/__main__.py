import os
import socket
import asyncio
import logging
import struct

from protocol.requests.buffer_pb2 import CreateBuffer, DestroyBuffer

from src.state import Client
from src.buffer import create_buffer, destroy_buffer
from src.errors import BadRequestError, InternalCompositorError

SOCKET_ADDRESS = os.environ["FABRIX_ADDR"]
REQUEST_HANDLERS = {
    0: (CreateBuffer, create_buffer),
    1: (DestroyBuffer, destroy_buffer),
}


async def main(address: str):
    logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.INFO)
    logging.info("compositor started")

    loop = asyncio.get_event_loop()

    with socket.socket(
        socket.AF_UNIX, socket.SOCK_STREAM | socket.SOCK_NONBLOCK
    ) as sock:
        sock.bind(address)
        sock.listen()

        while True:
            conn, _ = await loop.sock_accept(sock)
            asyncio.create_task(handle_client(conn))


async def handle_client(conn):
    logging.info("client connected")
    conn.setblocking(False)

    loop = asyncio.get_event_loop()
    state = Client()

    with conn:
        while True:
            data = await loop.sock_recv(conn, 1024)
            if not data:
                break

            (req_type,) = struct.unpack("I", data[:4])
            logging.debug("request received with type %d", req_type)

            cls, handler = REQUEST_HANDLERS[req_type]

            req = cls()
            req.ParseFromString(data[4:])
            logging.debug("request received:\n%s", req)

            try:
                handler(req, state)
            except BadRequestError:
                ...  # TODO: error handling
                # loop.sock_sendall(conn, )
            except InternalCompositorError:
                ...  # TODO: error handling

    logging.info("client disconnected")


if __name__ == "__main__":
    asyncio.run(main(SOCKET_ADDRESS))
