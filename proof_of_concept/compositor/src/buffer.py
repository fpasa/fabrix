from protocol.requests.buffer_pb2 import CreateBuffer, DestroyBuffer

from src.state import Client, Buffer
from src.errors import BadRequestError


def create_buffer(req: CreateBuffer, state: Client):
    if req.buffer_id in state.buffers:
        raise BadRequestError(f"A buffer with ID '{req.buffer_id}' already exists.")

    state.buffers[req.buffer_id] = Buffer()


def destroy_buffer(req: DestroyBuffer, state: Client):
    if req.id not in state.buffers:
        raise BadRequestError(f"Buffer with ID '{req.buffer_id}' was not found.")

    del state.buffers[req.buffer_id]
