from dataclasses import dataclass, field


@dataclass(frozen=True)
class Buffer:
    ...


@dataclass(frozen=True)
class Pool:
    ...


@dataclass(frozen=True)
class Client:
    memory_pools: dict[str, Pool] = field(default_factory=dict)
    buffers: dict[int, Buffer] = field(default_factory=dict)
